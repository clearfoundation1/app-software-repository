<?php

$lang['software_repository_add_note:'] = 'Для додатку «Магазин» потрібно ввімкнути таке сховище:';
$lang['software_repository_app_description'] = 'Додаток «Репозиторії програмного забезпечення» надає список сховищ, доступних для сервера. Програми, доступні в магазині, залежать від того, які сховища ввімкнено.';
$lang['software_repository_app_name'] = 'Репозиторії програмного забезпечення';
$lang['software_repository_app_tooltip'] = 'Увімкнення та/або вимкнення репозиторіїв може мати несприятливий вплив на стабільність системи. Внесення змін до налаштувань за замовчуванням рекомендується лише для досвідчених користувачів.';
$lang['software_repository_busy'] = 'Менеджер програмного забезпечення зайнятий...';
$lang['software_repository_cli_only'] = 'Тільки CLI';
$lang['software_repository_name'] = 'Ім`я';
$lang['software_repository_number_of_packages'] = 'Пакети';
$lang['software_repository_repo_list'] = 'Список репозиторіїв';
$lang['software_repository_warn_centos_disable'] = 'Подумайте про те, щоб вимкнути репозиторій "clearos-centos" - цей сервер має доступ до перевірених оновлень програмного забезпечення через платну службу або перші 30 днів встановлення Community.';
$lang['software_repository_warn_centos_enable'] = 'Подумайте про те, щоб увімкнути репозиторій "clearos-centos", щоб підтримувати вашу систему в актуальному стані.';
$lang['software_repository_warn_centos_updates_disable'] = 'Рекомендуємо вимкнути репозиторій "clearos-centos-updates".';
$lang['software_repository_warn_centos_updates_enable'] = 'Подумайте про те, щоб увімкнути репозиторій "clearos-centos-updates", щоб підтримувати вашу систему в актуальному стані.';
$lang['software_repository_warn_epel_duplicate'] = 'Подумайте про те, щоб вимкнути репозиторій "clearos-epel" - цей сервер має доступ до перевірених оновлень EPEL через платну службу або перші 30 днів встановлення Community.';
$lang['software_repository_warn_epel_is_enabled'] = 'Репозиторій EPEL (clearos-epel) увімкнено. Доступ до EPEL надається для зручності, але деякі пакунки або оновлення можуть спричинити нестабільність та/або майбутні проблеми з залежністю пакетів. Подумайте про відключення цього сховища.';  
$lang['software_repository_warn_fast_updates_enable'] = 'Розгляньте можливість увімкнення "clearos-fast-updates" - це сховище використовується для швидкого виведення оновлень, які вважаються критичними для безпеки вашої системи.';
$lang['software_repository_warn_test_repo'] = 'Подумайте про те, щоб вимкнути сховище "%s". Він призначений лише для розробників, бета-тестерів і досвідчених користувачів.';
$lang['software_repository_warn_updates_disable'] = 'Подумайте про те, щоб вимкнути репозиторій "clearos-updates" - цей сервер має доступ до перевірених оновлень програмного забезпечення через платну службу або перші 30 днів встановлення Community.';
$lang['software_repository_warn_updates_enable'] = 'Подумайте про те, щоб увімкнути репозиторій "clearos-updates"... він надає оновлення програмного забезпечення для виправлення помилок, додавання функцій і, в деяких випадках, для виправлення критичних вразливостей безпеки.';
$lang['software_repository_warning'] = 'Було визначено такі рекомендації/попередження:';
$lang['software_repository_updates_warning_event'] = 'Попередження про конфігурацію сховища програмного забезпечення';
